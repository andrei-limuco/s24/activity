//2
db.users.insertMany([{
        "firstName": "Stephen",
        "lastName": "Hawking",
        "age": 76.0,
        "email": "stephenhawking@mail.com",
        "department": "HR"
    },
    {
        "firstName": "Neil",
        "lastName": "Armstrong",
        "age": 82.0,
        "email": "neilarmstrong@mail.com",
        "department": "HR"
    },
    {
        "firstName": "Bill",
        "lastName": "Gates",
        "age": 65.0,
        "email": "billgates@mail.com",
        "department": "Operations"
    },
    {
        "firstName": "Jane",
        "lastName": "Doe",
        "age": 21.0,
        "email": "janedoe@mail.com",
        "department": "HR"
    },
]);


//3
db.users.find({
    $or: [{
            "firstName": {
                $regex: 's',
                $options: '$i'
            }
        },
        {
            "lastName": {
                $regex: 'd',
                $options: '$i'
            }
        }
    ]
}, {
    "firstName": 1,
    "lastName": 1,
    "_id": 0
});


//4
db.users.find({
    $and: [{
            "department": "HR"
        },
        {
            "age": {
                $gte: 70
            }
        }
    ]
});

//5
db.users.find({
    $and: [{
            "firstName": {
                $regex: 'e'
            }
        },
        {
            "age": {
                $lte: 30
            }
        }
    ]
});