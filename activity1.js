db.users.insertMany([{
        "firstName": "Diane",
        "lastName": "Murphy",
        "email": "dmurphy@mail.com",
        "isAdmin": false,
        "isActive": true
    },
    {
        "firstName": "Mary",
        "lastName": "Patterson",
        "email": "mpatterson@mail.com",
        "isAdmin": false,
        "isActive": true
    },
    {
        "firstName": "Jeff",
        "lastName": "Firrelli",
        "email": "jfirrelli@mail.com",
        "isAdmin": false,
        "isActive": true
    },
    {
        "firstName": "Gerard",
        "lastName": "Bondour",
        "email": "gbondour@mail.com",
        "isAdmin": false,
        "isActive": true
    },
    {
        "firstName": "Pamela",
        "lastName": "Castillo",
        "email": "pcastillo@mail.com",
        "isAdmin": true,
        "isActive": false
    },
    {
        "firstName": "George",
        "lastName": "Vanauf",
        "email": "gbvanauf@mail.com",
        "isAdmin": true,
        "isActive": false
    }
]);

db.courses.insertMany([{
        "name": "Professional Development",
        "price": 10000.0
    },
    {
        "name": "Business Processing",
        "price": 13000.0
    }
]);

db.users.find({ "isAdmin": false });

db.courses.updateOne({ "name": "Professional Development" }, { $set: { "enrollees": 6 } });

db.courses.updateOne({ "name": "Business Processing" }, { $set: { "enrollees": 6 } });

db.courses.updateOne({ "name": "Professional Development" }, { $set: { "enrollees": [{ "userID": ObjectId("620cc54c53d575657b12399f") }, { "userID": ObjectId("620cc54c53d575657b1239a0") }, { "userID": ObjectId("620cc54c53d575657b1239a1") }, { "userID": ObjectId("620cc54c53d575657b1239a2") }, { "userID": ObjectId("620cc54c53d575657b1239a3") }, { "userID": ObjectId("620cc54c53d575657b1239a4") }] } });

db.courses.updateOne({ "name": "Business Processing" }, { $set: { "enrollees": [{ "userID": ObjectId("620cc54c53d575657b12399f") }, { "userID": ObjectId("620cc54c53d575657b1239a0") }, { "userID": ObjectId("620cc54c53d575657b1239a1") }, { "userID": ObjectId("620cc54c53d575657b1239a2") }, { "userID": ObjectId("620cc54c53d575657b1239a3") }, { "userID": ObjectId("620cc54c53d575657b1239a4") }] } });